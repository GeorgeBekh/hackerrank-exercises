package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Complete the gridSearch function below.
func gridSearch(G []string, P []string) string {
	for i := 0; i < len(G) && len(G) - i >= len(P); i++ {
		if findPattern(G[i:], P) {
			return "YES"
		}
	}

	return "NO"
}

func findPattern(G []string, P []string) bool {
	offsets := make([]int, 0)
	//fmt.Println("ITERATION")

	for i := 0; i < len(P); i++ {
		if i >= len(G) {
			return false
		}

		foundIndexes := getBeginnings(
			G[i],
			P[i],
		)

		if len(offsets) != 0 {
			foundIndexes = getCommonElements(offsets, foundIndexes)
		}

		if len(foundIndexes) == 0 {
			return false
		}

		offsets = foundIndexes
	}

	return true
}

func getBeginnings(str, substr string) []int {
	beginnings := make([]int, 0)

	index := 0
	for i := 0; i < len(str); i++ {
		found := strings.Index(str[i:], substr)
		if found == -1 {
			break
		}

		if index == 0 || found != index {
			index = i + found
			beginnings = append(beginnings, i + found)
			i = i + found
		}
	}

	return beginnings
}

func getCommonElements(a []int, b []int) []int {
	commonElements := make([]int, 0)

	for _, aValue := range a {
		for _, bValue := range b {
			if aValue == bValue {
				commonElements = append(commonElements, aValue)
				break
			}
		}
	}

	return commonElements
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

	stdout := os.Stdout

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024 * 1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		RC := strings.Split(readLine(reader), " ")

		RTemp, err := strconv.ParseInt(RC[0], 10, 64)
		checkError(err)
		R := int32(RTemp)

		var G []string

		for i := 0; i < int(R); i++ {
			GItem := readLine(reader)
			G = append(G, GItem)
		}

		rc := strings.Split(readLine(reader), " ")

		rTemp, err := strconv.ParseInt(rc[0], 10, 64)
		checkError(err)
		r := int32(rTemp)

		var P []string

		for i := 0; i < int(r); i++ {
			PItem := readLine(reader)
			P = append(P, PItem)
		}

		result := gridSearch(G, P)

		fmt.Fprintf(writer, "%s\n", result)
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
