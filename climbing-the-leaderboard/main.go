package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strings"
)

// Complete the climbingLeaderboard function below.
func climbingLeaderboard(scores []int, alice []int) []int {
	cachePlace := 1
	scoreIndex := 0

	places := make([]int, 0)

	for i := len(alice) - 1; i >= 0; i-- {
		place := 0
		aliceScore := alice[i] // 120

		for ; scoreIndex < len(scores); scoreIndex++ {
			score := scores[scoreIndex]

			fmt.Printf("scoreIndex: %d\ncachePlace: %d\n", scoreIndex, cachePlace)

			if aliceScore >= score {
				fmt.Printf("Alice's %d is greater than %d\n", aliceScore, score)
				place = cachePlace
				break
			}

			if scoreIndex + 1 >= len(scores) {
				cachePlace++
				scoreIndex++
				break
			}

			if score != scores[scoreIndex + 1] {
				cachePlace++
			}

			fmt.Printf("Skipping %d\n", score)

		}

		if scoreIndex == len(scores) {
			place = cachePlace
		}


		places = append(places, int(place))
	}

	for i, j := 0, len(places)-1; i < j; i, j = i+1, j-1 {
		places[i], places[j] = places[j], places[i]
	}

	return places
}


func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
    checkError(err)

    defer stdout.Close()

    writer := bufio.NewWriterSize(stdout, 1024 * 1024)
    defer writer.Flush()

    var scoresCount int
    _, err = fmt.Fscan(reader, &scoresCount)
    checkError(err)

    var scores []int

    for i := 0; i < int(scoresCount); i++ {
        var scoresItem int
        _, err := fmt.Fscan(reader, &scoresItem)
        checkError(err)
        scores = append(scores, scoresItem)
    }

    var aliceCount int
    _, err = fmt.Fscan(reader, &aliceCount)

    checkError(err)

    var alice []int

    for i := 0; i < int(aliceCount); i++ {
        var aliceItem int
        _, err := fmt.Fscan(reader, &aliceItem)

        checkError(err)
        alice = append(alice, aliceItem)
    }

    result := climbingLeaderboard(scores, alice)

    for i, resultItem := range result {
        fmt.Fprintf(writer, "%d", resultItem)

        if i != len(result) - 1 {
            fmt.Fprintf(writer, "\n")
        }
    }

    fmt.Fprintf(writer, "\n")
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}
