package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

/*
 * Complete the 'pickingNumbers' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts INTEGER_ARRAY a as parameter.
 */

type arr []int32

func (a arr) Len() int {
	return len(a)
}

func (a arr) Less(i, j int) bool {
	return a[i] < a[j]
}
func (a arr) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func pickingNumbers(a []int32) int32 {
	if len(a) == 0 {
		return 0
	}
	var nums arr = arr(a)
	sort.Sort(nums)

	counters := make([]int, 2)
	for i := range counters {
		counters[i] = 1
	}
	counter := 0

	maxLength := 1

	for i, num := range nums {
		if i == 0 {
			continue
		}

		switch num - nums[i-1] {
		case 0:
			for i := range counters {
				counters[i]++
			}
		case 1:
			counters[counter]++
			counter = (counter + 1) % len(counters)
			counters[counter] = 1
		default:
			for i := range counters {
				counters[i] = 1
			}
		}

		for _, count := range counters {
			if count > maxLength {
				maxLength = count
			}
		}
	}

	return int32(maxLength)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)
	writer := bufio.NewWriterSize(os.Stdout, 16*1024*1024)

	nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	n := int32(nTemp)
	aTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var a []int32

	for i := 0; i < int(n); i++ {
		aItemTemp, err := strconv.ParseInt(aTemp[i], 10, 64)
		checkError(err)
		aItem := int32(aItemTemp)
		a = append(a, aItem)
	}

	result := pickingNumbers(a)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
