package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"math"
)

var magicSquare = [][]int32{[]int32{8,1,6}, []int32{3,5,7}, []int32{4,9,2}}

// TODO: Получить все варианты квадрата 3x3, получить разницу с каждым, вернуть минимальную.
func formingMagicSquare(s [][]int32) int32 {
	diffs := make([]int, 0)
	for _, variation := range getAllVariations() {
		diff := 0
		for ri, r := range s {
			for ci, c := range r {
				diff = diff + int(math.Abs(float64(c - variation[ri][ci])))
			}
		}
		diffs = append(diffs, diff)
	}

	min := diffs[0]
	for _, e := range diffs {
		if e < min {
			min = e
		}
	}


	return int32(min)
}

func getAllVariations() [][][]int32 {
	var variations [][][]int32

	variations = append(variations, magicSquare)
	rotatedMagicSquare := rotate(magicSquare)

	for i := 0; i < 2; i++ {
		variations = append(variations, rotatedMagicSquare)
		rotatedMagicSquare = rotate(rotatedMagicSquare)
	}

	variations = append(variations, rotatedMagicSquare)

	flippedMagicSquare := reverseColumns(magicSquare)
	variations = append(variations, flippedMagicSquare)
	rotatedMagicSquare = rotate(flippedMagicSquare)

	for i := 0; i < 2; i++ {
		variations = append(variations, rotatedMagicSquare)
		rotatedMagicSquare = rotate(rotatedMagicSquare)
	}

	variations = append(variations, rotatedMagicSquare)

	return variations

}

func rotate(m [][]int32) [][]int32 {
	return reverseColumns(transpose(m))
}

func transpose(m [][]int32) [][]int32 {
	newMatrix := [][]int32{make([]int32, 3), make([]int32, 3), make([]int32, 3)}
	for ri, r := range m {
		for ci, c := range r {
			newMatrix[ci][ri] = c
		}
	}

	return newMatrix
}

func reverseColumns(m [][]int32) [][]int32 {
	newMatrix := [][]int32{make([]int32, 3), make([]int32, 3), make([]int32, 3)}
	for ri, r := range m {
		for ci, c := range r {
			newMatrix[2-ri][ci] = c
		}
	}

	return newMatrix
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

	stdout := os.Stdout

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024 * 1024)

	var s [][]int32
	for i := 0; i < 3; i++ {
		sRowTemp := strings.Split(readLine(reader), " ")

		var sRow []int32
		for _, sRowItem := range sRowTemp {
			sItemTemp, err := strconv.ParseInt(sRowItem, 10, 64)
			checkError(err)
			sItem := int32(sItemTemp)
			sRow = append(sRow, sItem)
		}

		if len(sRow) != 3 {
			panic("Bad input")
		}

		s = append(s, sRow)
	}

	result := formingMagicSquare(s)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
