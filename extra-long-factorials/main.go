package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

/*
 * Complete the 'extraLongFactorials' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

func extraLongFactorials(n *big.Int) *big.Int {
	one := new(big.Int).SetInt64(1)
	if n.Cmp(one) == 0 {
		return one
	}
	temp := new(big.Int)
	return n.Mul(n, extraLongFactorials(temp.Sub(n, one)))
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	n := int64(nTemp)

	fmt.Printf("%s", extraLongFactorials(new(big.Int).SetInt64(n)))
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
