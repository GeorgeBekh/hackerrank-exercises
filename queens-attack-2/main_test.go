package main

import (
	"testing"
)


func TestMainDiagonal(t *testing.T) {
	result := mainDiagonal(4, 1, 3)

	if result != 0 {
		t.Errorf("%d != 0", result)
	}

	result = mainDiagonal(0, 1, 3)

	if result != 4 {
		t.Errorf("%d != 4", result)
	}

	result = mainDiagonal(1, 4, 0)

	if result != 3 {
		t.Errorf("%d != 3", result)
	}
}

func TestMainDiagonalRev(t *testing.T) {
	result := mainDiagonalRev(3, 2, 2)

	if result != 1 {
		t.Errorf("%d != 1", result)
	}

	result = mainDiagonalRev(4, 3, 2)

	if result != 1 {
		t.Errorf("%d != 1", result)
	}

	result = mainDiagonalRev(0, 0, 4)

	if result != 4 {
		t.Errorf("%d != 4", result)
	}
}

func TestMinorDiagonal(t *testing.T) {
	result := minorDiagonal(2, 1, 1)

	if result != 2 {
		t.Errorf("%d != 2", result)
	}

	result = minorDiagonal(3, 1, 4)

	if result != 6 {
		t.Errorf("%d != 6", result)
	}
}

func TestMinorDiagonalRev(t *testing.T) {
	result := minorDiagonalRev(5, 1, 4)

	if result != 2 {
		t.Errorf("%d != 2", result)
	}

	result = minorDiagonalRev(3, 1, 1)

	if result != 3 {
		t.Errorf("%d != 3", result)
	}
}
