package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/**
 * find y
 * |\00|
 * |0\0|
 * |00\|
 */
func mainDiagonal(x, q_x, q_y int32) int32 {
	return -x + q_x + q_y
}

/**
 * find x
 * |\00|
 * |0\0|
 * |00\|
 */
func mainDiagonalRev(y, q_x, q_y int32) int32 {
	return -y + q_x + q_y
}

/**
 * find y
 * |00/|
 * |0/0|
 * |/00|
 */
func minorDiagonal(x, q_x, q_y int32) int32 {
	return x + q_y - q_x
}

/**
 * find x
 * |00/|
 * |0/0|
 * |/00|
 */
func minorDiagonalRev(y, q_x, q_y int32) int32 {
	return y + q_x - q_y
}

// Complete the queensAttack function below.
func queensAttack(n int32, _ int32, q_y int32, q_x int32, obstacles [][]int32) int32 {
	minor := int32(0)
	main := n

	classifiedObstacles := make(map[string][]int32)

	queen_minor := q_y - q_x
	queen_main := q_y + q_x
	//fmt.Println(q_x, q_y)

	if queen_minor >= minor {
		obstacles = append(
			obstacles,
			[]int32{minorDiagonal(0, q_x, q_y), 0}, //A
			[]int32{n+1, minorDiagonalRev(n+1, q_x, q_y)}, //B
		)
	} else {
		obstacles = append(
			obstacles,
			[]int32{0, minorDiagonalRev(0, q_x, q_y)}, //A
			[]int32{minorDiagonal(n+1, q_x, q_y), n + 1}, //B
		)
	}

	if queen_main > main {
		obstacles = append(
			obstacles,
			[]int32{mainDiagonal(n+1, q_x, q_y), n+1}, //A
			[]int32{n+1, mainDiagonalRev(n+1, q_x, q_y)}, //B
		)
	} else {
		obstacles = append(
			obstacles,
			[]int32{0, mainDiagonalRev(0, q_x, q_y)}, //A
			[]int32{mainDiagonal(0, q_x, q_y), 0}, //B
		)
	}

	obstacles = append(
		obstacles,
		[]int32{n+1, q_x}, //TOP
		[]int32{0, q_x}, //BOTTOM
		[]int32{q_y, n+1}, //LEFT
		[]int32{q_y, 0}, //RIGHT
	)


	for i, obstacle := range obstacles {
		x := obstacle[1]
		y := obstacle[0]

		if x == q_x && y > q_y {
			//TOP
			if value, ok := classifiedObstacles["top"]; ok {
				if value[0] > y {
					classifiedObstacles["top"] = obstacle
				}
			} else {
				classifiedObstacles["top"] = obstacles[i]
			}
		} else if x == q_x && y < q_y {
			//BOTTOM
			if value, ok := classifiedObstacles["bottom"]; ok {
				if value[0] < y {
					classifiedObstacles["bottom"] = obstacle
				}
			} else {
				classifiedObstacles["bottom"] = obstacles[i]
			}
		} else if y == q_y && x > q_x {
			//RIGHT
			if value, ok := classifiedObstacles["right"]; ok {
				if value[1] > x {
					classifiedObstacles["right"] = obstacle
				}
			} else {
				classifiedObstacles["right"] = obstacles[i]
			}
		} else if y == q_y && x < q_x {
			//LEFT
			if value, ok := classifiedObstacles["left"]; ok {
				if value[1] < x {
					classifiedObstacles["left"] = obstacle
				}
			} else {
				classifiedObstacles["left"] = obstacles[i]
			}
		} else if x + y == q_x + q_y && x > q_x {
			//MAIN D RIGHT
			if value, ok := classifiedObstacles["mainRight"]; ok {
				if value[1] > x {
					classifiedObstacles["mainRight"] = obstacle
				}
			} else {
				classifiedObstacles["mainRight"] = obstacles[i]
			}
		} else if x + y == q_x + q_y && x < q_x {
			//MAIN D LEFT
			if value, ok := classifiedObstacles["mainLeft"]; ok {
				if value[1] < x {
					classifiedObstacles["mainLeft"] = obstacle
				}
			} else {
				classifiedObstacles["mainLeft"] = obstacles[i]
			}
		} else if x - y == q_x - q_y && y > q_y {
			//MINOR D RIGHT
			if value, ok := classifiedObstacles["minorRight"]; ok {
				if value[1] > x {
					classifiedObstacles["minorRight"] = obstacle
				}
			} else {
				classifiedObstacles["minorRight"] = obstacles[i]
			}
		} else if x - y == q_x - q_y && y < q_y {
			//MINOR D LEFT
			if value, ok := classifiedObstacles["minorLeft"]; ok {
				if value[1] < x {
					classifiedObstacles["minorLeft"] = obstacle
				}
			} else {
				classifiedObstacles["minorLeft"] = obstacles[i]
			}
		}
	}


	emptyCells := int32(0)

	emptyCells += classifiedObstacles["top"][0] - q_y - 1
	emptyCells += q_y - classifiedObstacles["bottom"][0] - 1
	emptyCells += classifiedObstacles["right"][1] - q_x - 1
	emptyCells += q_x - classifiedObstacles["left"][1] - 1

	emptyCells += classifiedObstacles["mainRight"][1] - q_x - 1
	emptyCells += q_x - classifiedObstacles["mainLeft"][1] - 1
	emptyCells += classifiedObstacles["minorRight"][1] - q_x - 1
	emptyCells += q_x - classifiedObstacles["minorLeft"][1] - 1

	return emptyCells
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

	stdout := os.Stdout

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024 * 1024)

	nk := strings.Split(readLine(reader), " ")

	nTemp, err := strconv.ParseInt(nk[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	kTemp, err := strconv.ParseInt(nk[1], 10, 64)
	checkError(err)
	k := int32(kTemp)

	r_qC_q := strings.Split(readLine(reader), " ")

	r_qTemp, err := strconv.ParseInt(r_qC_q[0], 10, 64)
	checkError(err)
	r_q := int32(r_qTemp)

	c_qTemp, err := strconv.ParseInt(r_qC_q[1], 10, 64)
	checkError(err)
	c_q := int32(c_qTemp)

	var obstacles [][]int32
	for i := 0; i < int(k); i++ {
		obstaclesRowTemp := strings.Split(readLine(reader), " ")

		var obstaclesRow []int32
		for _, obstaclesRowItem := range obstaclesRowTemp {
			obstaclesItemTemp, err := strconv.ParseInt(obstaclesRowItem, 10, 64)
			checkError(err)
			obstaclesItem := int32(obstaclesItemTemp)
			obstaclesRow = append(obstaclesRow, obstaclesItem)
		}

		if len(obstaclesRow) != int(2) {
			panic("Bad input")
		}

		obstacles = append(obstacles, obstaclesRow)
	}

	result := queensAttack(n, k, r_q, c_q, obstacles)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
