package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)

// Complete the dayOfProgrammer function below.
func dayOfProgrammer(year int32) string {
	if year == 1918 {
		return formatDate(26, 9, year)
	}

	if yearIsLeap(year) {
		return formatDate(12, 9, year)
	} else {
		return formatDate(13, 9, year)
	}
}

func formatDate(day int32, month int32, year int32) string {
	return fmt.Sprintf("%02d.%02d.%04d", day, month, year)
}

func yearIsLeap(year int32) bool {
	if year < 1918 {
		return year % 4 == 0
	} else {
		return year % 400 == 0 || year % 4 == 0 && year % 100 != 0
	}
}

func main() {
    reader := bufio.NewReaderSize(os.Stdin, 16 * 1024 * 1024)

    stdout := os.Stdout

    defer stdout.Close()

    writer := bufio.NewWriterSize(stdout, 16 * 1024 * 1024)

    yearTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
    checkError(err)
    year := int32(yearTemp)

    result := dayOfProgrammer(year)

    fmt.Fprintf(writer, "%s\n", result)

    writer.Flush()
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}
