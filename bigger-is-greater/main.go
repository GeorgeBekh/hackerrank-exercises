package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"sort"
)

func biggerIsGreater(w string) string {
	j := -1
	jVal := -1
	i := -1
	iVal := -1

	for index := 0; index < len(w) - 1; index++ {
		if w[index] < w[index + 1] {
			i = index + 1
			j = index
			iVal = int(w[index + 1])
			jVal = int(w[index])
		} else if int(w[index + 1]) > jVal && int(w[index + 1]) < iVal {
			i = index + 1
			iVal = int(w[index + 1])
		}
	}

	if i == -1 || j == -1 {
		return "no answer"
	}
	fmt.Println(string([]byte{uint8(jVal)}), string([]byte{uint8(iVal)}))

	result := []byte(w)
	result[j] = w[i]
	result[i] = w[j]

	ints := make([]int, 0)
	for _, char := range result[j+1:] {
		ints = append(ints, int(char))
	}

	result = result[:j+1]

	sort.Ints(ints)
	part := make([]byte, 0)

	for _, char := range ints {
		part = append(part, uint8(char))
	}
	result = append(result, part...)


	return string(result)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

	stdout := os.Stdout

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024 * 1024)

	TTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	T := int32(TTemp)

	for TItr := 0; TItr < int(T); TItr++ {
		w := readLine(reader)

		result := biggerIsGreater(w)

		fmt.Fprintf(writer, "%s\n", result)
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
