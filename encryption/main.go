package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"math"
)

// Complete the encryption function below.
func encryption(s string) string {
	sqrt := math.Sqrt(float64(len(s)))

	floor := int(math.Floor(sqrt))
	ceil := int(math.Ceil(sqrt))

	rows := floor
	columns := ceil

	if rows * columns < len(s) {
		rows = ceil
	}

	result := ""

	for columnI := 0; columnI < columns; columnI++ {
		for rowI := 0; rowI < rows; rowI++ {
			index := columnI + rowI * columns
			if index >= len(s) {
				continue
			}

			result += string(s[index])
		}

		result += " "
	}

	return result
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

	stdout := os.Stdout
	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024 * 1024)

	s := readLine(reader)

	result := encryption(s)

	fmt.Fprintf(writer, "%s\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
